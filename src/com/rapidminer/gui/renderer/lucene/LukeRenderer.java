package com.rapidminer.gui.renderer.lucene;

import java.awt.Component;
import java.awt.Panel;
import java.io.File;
import java.util.ArrayList;

import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexCommit;
import org.apache.lucene.index.IndexDeletionPolicy;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.MultiReader;
import org.apache.lucene.index.SegmentInfos;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.getopt.luke.KeepAllIndexDeletionPolicy;
import org.getopt.luke.KeepLastIndexDeletionPolicy;
import org.getopt.luke.Luke;

import com.rapidminer.gui.renderer.AbstractRenderer;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.operator.lucene.Index;
import com.rapidminer.report.Reportable;

public class LukeRenderer extends AbstractRenderer
{
	public LukeRenderer()
	{
		System.out.println("luke instanziated");
	}

	@Override
	public String getName()
	{
		// TODO Auto-generated method stub
		return "luke";
	}

	@Override
	public Component getVisualizationComponent(Object renderable, IOContainer ioContainer)
	{
		// TODO Auto-generated method stub
		if(renderable instanceof Index)
		{
			System.out.println("YAY, los geht das bitches!");
			Luke renderer = new Luke();
			renderer.openIndex(((Index)renderable).index);
			return renderer;
		}
		else
			return null;
	}

	@Override
	public Reportable createReportable(Object renderable, IOContainer ioContainer, int desiredWidth, int desiredHeight)
	{
		// TODO Auto-generated method stub
		return null;
	}

}

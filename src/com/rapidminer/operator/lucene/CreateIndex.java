package com.rapidminer.operator.lucene;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DoubleField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import com.rapidminer.operator.IOObjectCollection;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.text.Document;
import com.rapidminer.operator.text.Token;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeDirectory;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.conditions.EqualStringCondition;
import com.rapidminer.tools.Ontology;

public class CreateIndex extends Operator
{
	private InputPort documentCollection = getInputPorts().createPort("documents", IOObjectCollection.class);
	private OutputPort indexOutput = getOutputPorts().createPort("index");

	private final StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);

	public CreateIndex(OperatorDescription description) {
		super(description);
		// TODO Auto-generated constructor stub
	}

	public void doWork() throws OperatorException
	{
		IOObjectCollection data = documentCollection.getData(IOObjectCollection.class);
		IOObjectCollection<Document> documents = data;

		Directory index=null;
		if(this.getParameter("indextype").equals("RamIndex"))
		{
			index = new RAMDirectory();
		}
		else
		{
			try
			{
				index = new SimpleFSDirectory(this.getParameterAsFile("dir"));
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_4_9, analyzer);

		try
		{
			IndexWriter w = new IndexWriter(index, config);

			for(Document d : documents.getObjects())
			{
				org.apache.lucene.document.Document luceneDocument = new org.apache.lucene.document.Document();
				String text;
				StringBuilder buffer = new StringBuilder();
				for(Token token : d.getTokenSequence())
				{
					buffer.append(token.getToken());
					buffer.append(" ");
				}
				text = buffer.toString();
				luceneDocument.add(new TextField("body", text, Store.YES));

				for(String meta : d.getMetaDataKeys())
				{
					if(d.getMetaDataType(meta) == Ontology.STRING
							|| (d.getMetaDataType(meta) == Ontology.NOMINAL && d.getMetaDataValue(meta) instanceof String))
					{
						luceneDocument.add(new StringField(meta, (String) d.getMetaDataValue(meta), Store.YES));
					}
					else if(d.getMetaDataType(meta) == Ontology.REAL
							|| (d.getMetaDataType(meta) == Ontology.NUMERICAL && d.getMetaDataValue(meta) instanceof Double))
					{
						luceneDocument.add(new DoubleField(meta, (Double) d.getMetaDataValue(meta), Store.YES));
					}
					else
					{
						System.out.println("metadatakey " + meta + " vom  typ "
								+ Ontology.VALUE_TYPE_NAMES[d.getMetaDataType(meta)]);
					}
				}
				w.addDocument(luceneDocument);

			}

			w.commit();
			w.close();
			indexOutput.deliver(new Index(index));
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<ParameterType> getParameterTypes()
	{
		List<ParameterType> types = super.getParameterTypes();
		types.add(new ParameterTypeCategory("indextype", "The type of Index to be built", new String[] {
				"RamDirectory", "FileDirectory" }, 0));
		ParameterType file = new ParameterTypeDirectory("dir", "The directory to store the index in", false);
		file.registerDependencyCondition(new EqualStringCondition(this, "indextype", true, "FileDirectory"));
		types.add(file);
		return types;
	}

}

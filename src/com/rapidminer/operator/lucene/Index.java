package com.rapidminer.operator.lucene;
import org.apache.lucene.store.Directory;

import com.rapidminer.operator.ResultObjectAdapter;


public class Index extends ResultObjectAdapter{
	/**
	 *
	 */
	private static final long serialVersionUID = -4013824290948259293L;
	public Directory index;


	public Index(Directory index) {
		super();
		this.index = index;
	}



}

package com.rapidminer.operator.lucene;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.IndexableFieldType;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import com.rapidminer.operator.IOObjectCollection;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.Ontology;

public class SearchQuery extends Operator
{

	private InputPort inputIndex = getInputPorts().createPort("index", IOObjectCollection.class);
	private OutputPort documentsOutput = getOutputPorts().createPort("documents");

	private final StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
	private Directory index;

	public SearchQuery(OperatorDescription description) {
		super(description);
		// TODO Auto-generated constructor stub
	}

	public void doWork() throws UserError
	{
		Directory index = inputIndex.getData(Index.class).index;
		String query = this.getParameterAsString("query");


		IOObjectCollection<com.rapidminer.operator.text.Document> documents = new IOObjectCollection<com.rapidminer.operator.text.Document>();

		try
		{
			IndexReader indexReader = IndexReader.open(index);
			IndexSearcher searcher = new IndexSearcher(indexReader);

			Query q = new QueryParser(Version.LUCENE_4_9, "body", analyzer).parse(query);
			TopScoreDocCollector collector = TopScoreDocCollector.create(64, true);
			searcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
			String body = this.getParameterAsString("body");
			for(int i = 0; i < hits.length; ++i)
			{
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);

				com.rapidminer.operator.text.Document rmDocument = new com.rapidminer.operator.text.Document(d.get(body));
				for(IndexableField f : d.getFields())
				{
					if(f.name().equals(body)) continue;
					IndexableFieldType type = f.fieldType();

					if(type instanceof FieldType && (((FieldType) type).numericType()!=null))
					{
						rmDocument.addMetaData(f.name(),f.numericValue().doubleValue(),Ontology.REAL);
					}
					else
						rmDocument.addMetaData(f.name(),f.stringValue(),Ontology.STRING);
				}
				documents.add(rmDocument);
			}
			this.documentsOutput.deliver(documents);

		} catch (ParseException ex)
		{

		} catch (IOException ex)
		{

		}
	}

	@Override
	public List<ParameterType> getParameterTypes()
	{
		List<ParameterType> types = super.getParameterTypes();
		types.add(new ParameterTypeString("query", "The search Query in Lucene Syntax"));
		types.add(new ParameterTypeString("body", "The field used to index the contents of the files","body"));
		return types;
	}

}

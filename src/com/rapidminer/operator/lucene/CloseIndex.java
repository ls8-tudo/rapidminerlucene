package com.rapidminer.operator.lucene;

import java.io.IOException;

import com.rapidminer.operator.IOObjectCollection;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.ports.InputPort;

public class CloseIndex extends Operator
{

	public CloseIndex(OperatorDescription description) {
		super(description);
	}

	private InputPort inputIndex = this.getInputPorts().createPort("index", Index.class);

	public void doWork()
	{
		try
		{
			((Index)inputIndex.getData(Index.class)).index.close();
			System.out.println("Directory Index closed");
		} catch (UserError e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

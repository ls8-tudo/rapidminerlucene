package com.rapidminer.operator.lucene;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.store.SimpleFSDirectory;

import com.rapidminer.operator.IOObjectCollection;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeDirectory;
import com.rapidminer.parameter.conditions.EqualStringCondition;

public class OpenIndex extends Operator
{

	public OpenIndex(OperatorDescription description) {
		super(description);
	}

	private OutputPort outputIndex = this.getOutputPorts().createPort("index");

	public void doWork()
	{
		try
		{
			outputIndex.deliver(new Index(new SimpleFSDirectory(this.getParameterAsFile("dir"))));
		} catch (UserError e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<ParameterType> getParameterTypes()
	{
		List<ParameterType> types = super.getParameterTypes();
		ParameterType file = new ParameterTypeDirectory("dir", "The directory to open the index from", false);
		types.add(file);
		return types;
	}

}
